$(document).ready(function() {

	function showComment() {
		$.ajax({
			url: '/comment',
			method: 'GET'
		}).done(function(result) {
			var targetDiv = $('#viewGuestbook');
			targetDiv.html('');
			for (var i = 0; i < result.length; i++) {
				var well = $('<div class="well"></div>');
				well.append('ID: ' + result[i].id + '<br>Name: ' + result[i].name + '<br>Email: ' + result[i].email + '<br> Comment: ' + result[i].comment + '<br>Date/Time: ' + result[i].createdAt + '<br>');
				targetDiv.append(well);
			}
		});
	}

	function searchComment() {
		var name = $('#nameSearch').val();
		$.ajax({
			url: '/comment/findByName',
			method: 'GET',
			data : { name : name }
		}).done(function(result) {
			var targetDiv = $('#viewGuestbook');
			targetDiv.html('');
			for (var i = 0; i < result.length; i++) {
				var well = $('<div class="well"></div>');
				well.append('ID: ' + result[i].id + '<br>Name: ' + result[i].name + '<br>Email: ' + result[i].email + '<br> Comment: ' + result[i].comment + '<br>Date/Time: ' + result[i].createdAt + '<br>');
				targetDiv.append(well);
			}
		});
	}
	showComment();
	$('#submitComment').on('click', function() {
		var name = $('#name').val();
		var email = $('#email').val();
		var comment = $('#comment').val();
		$.ajax({
			url: '/comment',
			method: 'POST',
			data: {
				name: name,
				email: email,
				comment: comment
			}
		}).success(function() {
			alert("Comment added.");
			$('#name').val('');
			$('#email').val('');
			$('#comment').val('');
		}).fail(function() {
			alert('Fail to add comment.');
		});
	});
	$('#searchButton').on('click', searchComment);
});