/**
 * CommentController
 *
 * @description :: Server-side logic for managing comments
 * @help        :: See http://links.sailsjs.org/docs/controllers
 */

module.exports = {
	findByName : function(req,res){
		var name = req.param('name');
		Comment.find({name : name}).exec(function(err,result){
			if(err){
				return res.send(err);
			}
			return res.json(result);
		});
	}
};

